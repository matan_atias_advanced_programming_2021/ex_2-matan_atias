#include "Ribosome.h"
#include <iostream>
#include <string>
using std::string;

/*
	Function gets RNA and returns the compatible protein
	Input:
		RNA 
	Output:
		compatible protein
*/
Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	Protein* protein = new Protein;
	string nucleotides = "";
	AminoAcid aminoDesc;
	//declaring vars
	protein->init();
	while (RNA_transcript.length() >= 3)
	{
		nucleotides = RNA_transcript.substr(0, 3);
		RNA_transcript = RNA_transcript.substr(3, RNA_transcript.length());
		aminoDesc = get_amino_acid(nucleotides);
		if (aminoDesc == UNKNOWN)
		{
			protein->clear();
			return nullptr;
		}
		protein->add(aminoDesc);
	}
	return protein;
}
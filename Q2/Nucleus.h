#pragma once

#include <string>
using std::string;


class Gene
{
	public:
		//methods
		void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
		unsigned int getStart() const;
		unsigned int getEnd() const;
		bool is_on_complementary_dna_strand() const;
		void setStart(unsigned int newStart);
		void setEnd(unsigned int newEnd);
		void set_on_complementary_dna_strand(bool new_on_complementary_dna_strand);

	private:
		//fields
		unsigned int _start;
		unsigned int _end;
		bool _on_complementary_dna_strand;
}; 


class Nucleus
{
	public:
		//methods
		void init(const string dna_sequence);
		string get_RNA_transcript(const Gene& gene) const;
		string get_reversed_DNA_strand() const;
		unsigned int get_num_of_codon_appearances(const std::string& codon) const;
	private:
		//fields
		string _DNA_strand;
		string _complementary_DNA_strand;
};

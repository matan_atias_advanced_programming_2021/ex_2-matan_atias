#include "Mitochondrion.h"
#include <iostream>
#include <string>
using std::string;


/*
	Function initiates the mitochondrion
	Input:
		None
	Output:
		None
*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}


/*
	Function checks if a protein is in a specific order
	Input:
		refrence to protein
	Output:
		None
*/
void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	int correctOrder[] = { ALANINE,LEUCINE,GLYCINE,HISTIDINE,LEUCINE,PHENYLALANINE,AMINO_CHAIN_END };
	int i = 0;
	AminoAcidNode* curr = protein.get_first();
	//declaring vars
	for (i = 0; i < 7; i++)
	{
		if (curr->get_data() != correctOrder[i])
		{
			this->_has_glocuse_receptor = false;
			i = 10;
		}
	}
	if (i != 10)
	{
		this->_has_glocuse_receptor = true;
	}
}


/*
	Function sets a new glocuse level
	Input:
		new glocuse units
	Output:
		None
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}


/*
	Functions checks if mitochondrion can produce ATP
	Input:
		None
	Output:
		true or false
*/
bool Mitochondrion::produceATP() const
{
	if (this->_has_glocuse_receptor == true && this->_glocuse_level >= 50)
	{
		return true;
	}
	else
	{
		return false;
	}
}

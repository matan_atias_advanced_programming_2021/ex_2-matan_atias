#include "Nucleus.h"
#include <iostream>
#include <string>
using std::string;

/*
	Function initiates the gene
	Input:
		start,end,on_complementary_dna_strand
	Output:
		None
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}


/*
	Function returns _start
	Input:
		None
	Output:
		_start
*/
unsigned int Gene::getStart() const
{
	return this->_start;
}


/*
	Function return _end
	Input:
		None
	Output:
		_end(to main)
*/
unsigned int Gene::getEnd() const
{
	return this->_end;
}


/*
	Function return _on_complementary_dna_strand
	Input:
		None
	Output:
		_on_complementary_dna_strand(to main)
*/
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}


/*
	Function changes _start
	Input:
		New _start value
	Output:
		None*/
void Gene::setStart(unsigned int newStart)
{
	this->_start = newStart;
}


/*
	Function changes _end
	Input:
		New _end value
	Output:
		None
*/
void Gene::setEnd(unsigned int newEnd)
{
	this->_end = newEnd;
}


/*
	Function changes _on_complementary_dna_strand
	Input:
		New _on_complementary_dna_strand 
	Output:
		None
*/
void Gene::set_on_complementary_dna_strand(bool new_on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = new_on_complementary_dna_strand;
}


/*
	Function gets dna sequence and put it in _DNA_strand inside the class
	Input:
		dna_sequence
	Output:
		None
*/
void Nucleus::init(const string dna_sequence)
{
	string dna = "";
	unsigned int i = 0;
	//declaring vars
	this->_DNA_strand = dna_sequence;
	for (i = 0; i < dna_sequence.length(); i++)
	{
		switch (dna_sequence[i])
		{
			case 'G':
				dna += 'C';
				break;
			case 'C':
				dna += 'G';
				break;
			case 'A':
				dna += 'T';
				break;
			case 'T':
				dna += 'A';
				break;
			default:
				std::cerr << "Invalid DNA sequence";
				exit(1);
		}
	}
	this->_complementary_DNA_strand = dna;
}


/*
	Function returns RNA to a given gene
	Input:
		gene
	Output:
		RNA
*/
string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string RNA = "";
	unsigned int i = 0;
	//declaring vars
	if (gene.is_on_complementary_dna_strand())
	{
		RNA = this->_complementary_DNA_strand.substr(gene.getStart(), gene.getEnd());
		for (i = 0; i < RNA.length(); i++)
		{
			if (RNA[i] == 'T')
			{
				RNA[i] = 'U';
			}
		}
	}
	return RNA;
}


/*
	Function returns reversed DNA strand
	Input:
		None
	Output:
		reversed DNA strand
*/
string Nucleus::get_reversed_DNA_strand() const
{
	string reversedDNA = this->_DNA_strand;
	//declaring vars
	std::reverse(reversedDNA.begin(), reversedDNA.end());
	return reversedDNA;
}


/*
	Function returns the amount of times a condon appears in a DNA strand
	Input:
		condon
	Output:	
		amount of times a condon appears in a DNA strand
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int counter = 0;
	unsigned int i = 0;
	string check = "";
	//declaring vars
	for (i = 0; i < this->_DNA_strand.length(); i += 3)
	{
		check += this->_DNA_strand[i];
		check += this->_DNA_strand[i + 1];
		check += this->_DNA_strand[i + 2];
		if (check == codon)
		{
			counter++;
		}
		check = "";
	}
	return counter;
}
#pragma once
#include <string>
#include "Protein.h"
using std::string;


class Mitochondrion
{
	public:
		void init();
		void insert_glucose_receptor(const Protein & protein);
		void set_glucose(const unsigned int glocuse_units);
		bool produceATP() const;
	private:
		//fields
		unsigned int _glocuse_level;
		bool _has_glocuse_receptor;
};


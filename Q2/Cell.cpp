#include "Cell.h"
#include <iostream>
#include <string>
using std::string;


/*
	Function initiates the cell
	Input:
		dna sequence, glocuse receptor gene
	Output:
		None
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
	this->_glocus_receptor_gene.init(glucose_receptor_gene.getStart(), glucose_receptor_gene.getEnd(), glucose_receptor_gene.is_on_complementary_dna_strand());
}


/*
	Function gets ATP(checks if cell is functional)
	Input:
		None
	Output:
		true if cell is functional, false if not
*/
bool Cell::get_ATP()
{
	string RNA = "";
	Protein* protein;
	bool flag = true;
	//declaring vars
	RNA = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	protein = _ribosome.create_protein(RNA);
	if (NULL == protein)
	{
		std::cerr << "Invalid protein";
		exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*protein);
	this->_mitochondrion.set_glucose(60);
	flag = this->_mitochondrion.produceATP();
	if (flag)
	{
		this->_atp_units = 100;
		return true;
	}
	return false;
}